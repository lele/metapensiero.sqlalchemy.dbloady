# -*- coding: utf-8 -*-
# :Project:   metapensiero.sqlalchemy.dbloady -- Property based attributes test
# :Created:   mer 07 giu 2017 14:24:00 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2017 Lele Gaifax
#

from setuptools import setup

setup(name='testdbloady',
      version='1.0',
      py_modules=['model'])
