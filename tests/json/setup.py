# -*- coding: utf-8 -*-
# :Project:   metapensiero.sqlalchemy.dbloady -- Test package
# :Created:   mar 08 nov 2016 09:56:04 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

from setuptools import setup

setup(name='testdbloady',
      version='1.0',
      py_modules=['model'])
