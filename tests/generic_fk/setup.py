# -*- coding: utf-8 -*-
# :Project:   metapensiero.sqlalchemy.dbloady -- Test package
# :Created:   sab 29 ott 2016 00:25:59 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

from setuptools import setup

setup(name='testdbloady',
      version='1.0',
      py_modules=['model'])
