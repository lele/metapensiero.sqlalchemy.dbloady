# -*- coding: utf-8 -*-
# :Project:   metapensiero.sqlalchemy.dbloady -- M2M test
# :Created:   sab 18 gen 2020, 08:39:44
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Lele Gaifax
#

from setuptools import setup

setup(name='testdbloady',
      version='1.0',
      py_modules=['model'])
