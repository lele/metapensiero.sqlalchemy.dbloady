.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- Composite PK test notes
.. :Created:   lun 14 nov 2016 23:46:11 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

=========================
 Composite PK test notes
=========================

This test checks that the loader is able to handle composite primary keys.
