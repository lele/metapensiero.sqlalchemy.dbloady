.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- Fields test notes
.. :Created:   lun 07 nov 2016 10:13:14 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

===================
 Fields test notes
===================

This simple test exercises the alternative syntax to enter instances data, the one that uses
tuples instead of dictionaries.
